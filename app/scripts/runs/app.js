/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhSdp')
/**
 * Defines toolbars and actions
 */
.run(function($app, $toolbar, $sidenav, $rootScope, $navigator, $actions) {

	$rootScope.app.config.local = $rootScope.app.config.local || {};
	var lang = $rootScope.app.setting.language || $rootScope.app.config.local.language || 'fa';

	// Toolbars
	$toolbar.newToolbar({
		id : 'amh-sdp.top-toolbar',
		title : 'Top toolbar',
		description : 'AMH-SDP Top toolbar',
		controller: 'AmhSdpToolbarTopCtrl',
		templateUrl : 'views/toolbars/top-toolbar.html',
		priority: 9,
		raw: true,
	});
	$toolbar.newToolbar({
		id : 'amh-sdp.main-toolbar',
		title : 'Main toolbar',
		description : 'Main AMH-SDP toolbar',
		controller: 'AmhSdpToolbarMainCtrl',
		templateUrl : 'views/toolbars/main-toolbar.html',
		priority: 8,
		raw: true,
	});

	// Sidenavs
	$sidenav.newSidenav({
		id : 'amh-sdp.main-sidenav',
		title : 'AMH SDP Main Sidenav',
		description : 'AMH SDP Main Sidenav',
		controller: 'AmhSdpSidenavMainCtrl',
		templateUrl : 'views/sidenavs/main-sidenav.html',
		locked : false,
		position : 'start',
	});

	// Actions
	$actions.newAction({
		id: 'amh-sdp.login',
		title : 'sign in',
		description : 'Login to site',
		helpId: 'user.login',
		icon : 'input',
		priority : 5,
		visible : function () {
			return $rootScope.app.user.anonymous;
		},
		action : function () {
			$navigator.openPage('users/login');
		},
		groups:['amhsdp.toolbar.top', 'amhsdp.user-space']
	});
	$actions.newAction({
		id: 'amh-sdp.logout',
		title : 'sign out',
		description : 'Logout from site',
		helpId: 'user',
		icon : 'power_settings_new',
		priority : 5,
		visible : function () {
			return !$rootScope.app.user.anonymous;
		},
		action : function () {
			$app.logout()//
			.then(function(){					
				$navigator.openPage('users/login');
			});
		},
		groups:['amhsdp.toolbar.top', 'amhsdp.user-space']
	});
	$actions.newAction({
		id: 'amh-sdp.profile',
		title : 'Profile',
		description : 'Go to user profile',
		helpId : 'user',
		icon : 'contacts',
		priority : 6,
		visible : function () {
			return !$rootScope.app.user.anonymous;
		},
		action : function () {
			$navigator.openPage('users/profile');
		},
		groups:['amhsdp.toolbar.top', 'amhsdp.user-space']
	});
	$actions.newAction({
		id: 'amh-sdp.account',
		title : 'User Account',
		description : 'Go to user account',
		helpId : 'user',
		icon : 'account_circle',
		priority : 6,
		visible : function () {
			return !$rootScope.app.user.anonymous;
		},
		action : function () {
			$navigator.openPage('users/account');
		},
		groups:['amhsdp.toolbar.top', 'amhsdp.user-space']
	});
	
	$actions.newAction({
		id: 'amh-sdp.home',
		title : 'home',
		description : 'go to home page',
		icon : 'home',
		priority : 20,
		link: '/',
		groups:['amhsdp.toolbar.top', 'amhsdp.sidenav.main']
	});
	$actions.newAction({
		id: 'amh-sdp.about-us',
		title : 'about us',
		description : 'about us',
		icon : 'info',
		priority : 19,
		link: 'content/about-us-' + lang,
		groups:['amhsdp.toolbar.top', 'rfmenu','amhsdp.sidenav.main']
	});
	$actions.newAction({
		id: 'amh-sdp.contact-us',
		title : 'contact us',
		description : 'contact us',
		icon : 'contact_phone',
		priority : 18,
		link: 'content/contact-us-' + lang,
		groups:['amhsdp.toolbar.top', 'rfmenu','amhsdp.sidenav.main']
	});
	$actions.newAction({
		id: 'amh-sdp.special-services',
		title : 'special services',
		description : 'special services',
		icon : 'gift',
		priority : 17,
		link: 'content/special-services-' + lang,
		groups:['amhsdp.toolbar.top']
	});
	$actions.newAction({
		id: 'amh-sdp.terms-of-service',
		title : 'terms of service',
		description : 'terms of service',
		icon : 'chat',
		priority : 16																																																																																																																																																																														,
		link: 'content/terms-of-service-' + lang,
		groups:['rfmenu']
	});
	$actions.newAction({
		id: 'amh-sdp.faq',
		title : 'help and faq',
		description : 'help & frequently asked questions',
		icon : 'help',
		priority : 15,
		link: 'content/help-and-faq-' + lang,
		groups:['rfmenu', 'amhsdp.sidenav.main']
	});
	
	$actions.newAction({
		id: 'amh-sdp.management-dashboard',
		title : 'management panel',
		description : 'go to management panel',
		icon : 'adjust',
		priority : 10,
		visible: function () {
			return $rootScope.app.user.owner;																																																																																																																				
		},
		link: '/saasdm-cpanel/',
		groups:['rfmenu']
	});
        $actions.newAction({
		id: 'amh-sdp.downloaded-files',
		title : 'downloaded files',
		description : 'link of downloaded files',
		icon : 'cloud_download',
		priority : 10,
		visible : function () {
			return !$rootScope.app.user.anonymous;
		},
		link: 'downloads-link',
		groups:['amhsdp.toolbar.top', 'amhsdp.user-space']
	});

});

