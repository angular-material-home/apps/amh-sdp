/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhSdp')

.run(function($widget) {

	$widget.newWidget({
		type: 'SdpPhoenixFooter',
		templateUrl : 'views/widgets/footer.html',
		title : 'Footer',
		label : 'Footer',
		description : 'Predigned footer used in SaaSDM-Phoenix theme',
		controller: 'SaaSDMPhoenixFooterCtrl',
		setting:[],
	});
	// Asset Slider
	$widget.newWidget({
		type: 'AssetSlider',
		templateUrl : 'views/widgets/sdp-asset-slider.html',
		title : 'Item Slider',
		label : 'Item Slider',
		description : 'A slider to show some items slide by slide.',
		help : 'http://dpq.co.ir',
		controller: 'SdpAssetSliderCtrl',
		setting:['item-slider', 'asset-query']
	});

});
