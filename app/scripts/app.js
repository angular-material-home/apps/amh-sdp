'use strict';

/**
 * @ngdoc overview
 * @name amhSdp
 * @description # amhSdp
 * 
 * Main module of the application.
 */
angular.module('amhSdp', [
	'mblowfish-language',
	
    'ngMaterialHomeUser',//
	'ngMaterialHomeSpa',//
    'ngMaterialHomeTheme',//
	'ngMaterialHomeBank',//
	
    'am-wb-common',//
    'am-wb-carousel',//
    'am-wb-mailchimp',//
    'am-wb-seen-collection'
])//
.config(function($localStorageProvider) {
	$localStorageProvider.setKeyPrefix('amhSdp.');
})
.run(function ($app, $toolbar, $sidenav) {
	// start the application
	$toolbar.setDefaultToolbars([
		'amh.owner-toolbar', 
		'amh-sdp.top-toolbar', 
		'amh-sdp.main-toolbar'
	]);
    $sidenav.setDefaultSidenavs(['amh-sdp.main-sidenav']);
	$app.start('app-amh-sdp');
});
