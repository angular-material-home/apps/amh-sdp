/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')
/*
 * ماشین حالت نرم افزار
 */
.config(function($routeProvider) {
	$routeProvider//
	.when('/assets', {
		templateUrl : 'views/folder.html',
		controller : 'AssetsCtrl'
	})//
	.when('/assets/:query', {
		templateUrl : 'views/folder.html',
		controller : 'AssetsCtrl'
	})//
	.when('/folder/:folderId', {
		templateUrl : 'views/folder.html',
		controller : 'FolderCtrl'
	})//
	.when('/category/:categoryId', {
		templateUrl : 'views/category.html',
		controller : 'CategoryCtrl'
	})//
	.when('/asset/:assetId', {
		templateUrl : 'views/asset.html',
		controller : 'AssetCtrl'
	})//
	.when('/asset/:assetId/link/:secureId', {
		templateUrl : 'views/link.html',
		controller : 'LinkCtrl'
	})//

	/*
	 * Customize some user scope pages.
	 */
	.when('/users/login', {
		templateUrl : 'views/customize/login.html',
		controller : 'MbAccountCtrl',
		protect : false
	})//
	.when('/users/reset-password', {
		templateUrl : 'views/customize/forgot-password.html',
		controller : 'MbPasswordCtrl'
	})//
	.when('/users/signup', {
		templateUrl : 'views/customize/signup.html',
		controller : 'SignupCtrl'
	})//
	.when('/users/profile', {
		templateUrl : 'views/customize/profile.html',
		controller : 'ProfileCtrl',
		protect : true
	})//
	// .when('/users/user-area', {
	// templateUrl : 'views/customize/user-area.html',
	// controller : 'UserAreaCtrl',
	// protect: true
	// })//

	.when('/events/:name', {
		templateUrl : 'views/collection.html',
		controller : 'CollectionCtrl'
	})//
    .when('/downloads-link' , {
        templateUrl : 'views/asset/downloaded-files-link.html',
        controller : 'DownloadedFilesLinkCtrl'
    })//
	.otherwise({
		templateUrl : 'views/page-not-found.html',
		controller : 'SdpPageNotFoundCtrl'
	});
});
