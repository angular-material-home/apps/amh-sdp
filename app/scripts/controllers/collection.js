/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:CollectionCtrl
 * @description # CollectionCtrl Controller of the amhSdp
 * 
 * این کنترلر وظیفه مدیریت فهرستی از سندها را به عنوان پست بر عهده دارد.
 */
.controller('CollectionCtrl', function($scope, $collection, $app, $routeParams, QueryParameter) {

	var lastPage = null;

	function nextPage() {
		if ($scope.working) {
			return;
		}
		if (lastPage && !lastPage.hasMore()) {
			return;
		}
		if (lastPage) {
			$scope.pp.setPage(lastPage.next());
		} else {
			$scope.pp.setPage(1);
		}
		$scope.working = true;

		// load documents
		$scope.collection.documents($scope.pp)//
		.then(function(docPage) {
			$scope.documents = $scope.documents.concat(docPage.items);
			$scope.working = false;
			lastPage = docPage;
		}, function(error) {
			print('Fail to get documents: ' + error.data.message);
			$scope.working = false;
		})//
		.finally(function() {
			window.prerenderReady = true;
		});
	}

	function load() {
		$scope.documents = [];
		$scope.realPage = true;
		lastPage = null;
		var name = $routeParams.name ? decodeURIComponent($routeParams.name) : null;
		if(name === null){
			$scope.message = 'undefined collection';
		}
		$scope.colLoading = $scope.working = true;
		$collection.collection(name)//
		.then(function(col){
			$scope.collection = col;
			if ($scope.collection !== null){
				$scope.setSeoSetting('collection', $scope.collection);
			}
			$scope.message = null;
			$scope.colLoading = $scope.working = false;
			nextPage();
		}, function(error){
			$scope.message = 'collection could not be load';
			console.log('Fail to load collection: ' + error.data.message);
			$scope.error = error.status;
			$scope.colLoading = $scope.working = false;
		});
	}

	$app
	.scopeMenu($scope)//
	.add({ // add new page
		priority : 15,
		icon : 'add_box',
		label: 'Create new content',
		visible : function() {
			return $scope.app.user.owner;
		},
		action : $scope.createNewContent
	});
	
	$scope.collection = null;
	$scope.documents = [];
	$scope.pp = new QueryParameter();
	$scope.pp.setOrder('id', 'd');
	$scope.colLoading = false;
	$scope.working = false;
	$scope.nextPage = nextPage;
	$scope.error = 0;

	load();

});
