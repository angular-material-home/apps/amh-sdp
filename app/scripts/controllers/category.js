/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:CategoryCtrl
 * @description # CategoryCtrl Controller of the amhSdp
 * 
 */
.controller('CategoryCtrl',	function($scope, $rootScope, $q, $sdp, $routeParams, $actions, QueryParameter) {

	var ctrl = {};
	
	
	function loadSubcategories() {
		if(ctrl.loadingChilds){
			return false;
		}
		ctrl.loadingChilds = true;
		var ppc = new QueryParameter();
                
		ppc.setFilter('parent', $routeParams.categoryId);
		return $sdp.categories(ppc)//
		.then(function(subCats) {
			$scope.categories = $scope.categories.concat(subCats.items);
		}, function() {
			ctrl.loadChildsState = 'fail';
		})//
		.finally(function(){
			ctrl.loadingChilds = false;
		});
	}

	function nextPage() {
		if ($scope.working) {
			return;
		}
		if (lastPage && !lastPage.hasMore()) {
			return;
		}
		if (lastPage) {
			$scope.pp.setPage(lastPage.next());
		} else {
			$scope.pp.setPage(1);
		}
		$scope.working = true;

		// find assets
		$scope.category.assets($scope.pp)//
		.then(function(assetList) {
			$scope.assets = $scope.assets.concat(assetList.items);
			$scope.working = false;
			lastPage = assetList;
			return assetList;
		}, function(error) {
			print('Fail to get assets: ' + error.data.message);
			$scope.working = false;
		});
	}

	/**
	 * جستجوی دارایی‌ها
	 * 
	 * @param query
	 * @returns
	 */
	function find(query) {
		$scope.pp.setQuery(query);
		reload();
	}

	function reload() {
		$scope.assets = [];
		lastPage = null;
		nextPage();
	}

	function load() {
		if($scope.working){
			return false;
		}
		$scope.content = {};
		var categoryId;
		if ($routeParams.categoryId) {
			categoryId = $routeParams.categoryId;
		} else {
			// error
			return;
		}

		$scope.working = true;

		return $sdp.category(categoryId)//
		.then(function(category) {
			$scope.category = category;
			$scope.working = false;
			$scope.error = 0;
			return $scope.category;
		}, function(error) {
			console.log('Fail to get category: ' + error.data.message);
			$scope.error = error.status;
			$scope.working = false;
		})//
		.then(function(category) {
			if (typeof category === 'undefined'){
				return;
			}
			$scope.setSeoSetting('category', $scope.category);
			$scope.loadContentOfCategory(category)//
			.finally(function(){
				window.prerenderReady = true;
			});
			
		})//
		.then(function() {
			loadSubcategories();
			nextPage();
		});
	}

	function advancedSearch() {
		// $scope.pp = new QueryParameter();
		if ($scope.searchParam.filterBy && $scope.searchParam.filterValue) {
			$scope.pp.setFilter($scope.searchParam.filterBy,
					$scope.searchParam.filterValue);
		} else {
			$scope.pp.setFilter(null, null);
		}
		if ($scope.searchParam.sortBy && $scope.searchParam.sortType) {
			$scope.pp.setOrder($scope.searchParam.sortBy,
					$scope.searchParam.sortType);
		} else {
			$scope.pp.setOrder();
		}
		if ($scope.searchParam.searchWord) {
			$scope.pp.setQuery($scope.searchParam.searchWord);
		} else {
			$scope.pp.setQuery();
		}
		reload();
	}

	var lastPage = null;
	var searchParam = {
			searchWord : '',
			filterBy : '',
			filterValue : '',
			sortBy : '',
			sortType : 'd'
	};
	
	/*
	 * Add scope menus
	 */
	$actions.newAction({ // edit content
		id: 'amhsdp.scope.edit_content',
		priority : 15,
		icon : 'edit',
		label: 'Edit content',
		title: 'Edit content',
		description : 'Toggle edit mode of the current contet',
		visible : function() {
			return $rootScope.app.user.owner;
		},
		action : $scope.toggleEditable,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	
	$actions.newAction({ // add new content
		id: 'amhsdp.scope.new_content',
		priority : 15,
		icon : 'add_box',
		label: 'Create new content',
		title: 'Create new content',
		description : 'Create new content',
		visible : function() {
			return $rootScope.app.user.owner;
		},
		action : $scope.createNewContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	$actions.newAction({ // save content
		id: 'amhsdp.scope.save_content',
		priority : 10,
		icon : 'save',
		label: 'Save current changes',
		title: 'Save current changes',
		description: 'Save current changes',
		visible : function() {
			return $rootScope.app.user.owner;
		},
		action : $scope.saveContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});

	$scope.ctrl = ctrl;
	
	$scope.content = {};
	$scope.categoryId = $routeParams.categoryId;
	$scope.error = 0;
	$scope.assets = [];
	$scope.categories = [];
	$scope.chain = [];
	$scope.pp = new QueryParameter();
	$scope.pp.setOrder('id', 'd');
	$scope.working = false;
	$scope.fetchingCategories = false;

	$scope.searchParam = searchParam;
	$scope.load = load;
	$scope.search = find;
	$scope.nextPage = nextPage;
	$scope.advancedSearch = advancedSearch;

	load();

	$scope.$watch('contentValue', function(){
		if($scope.contentValue !== undefined && $scope.contentValue !== null){
			$scope.setSeoSetting('content', $scope.contentValue);
		}
	}, true);
});
