/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:SaaSDMPhoenixFooterCtrl
 * @description # SaaSDMPhoenixFooterCtrl Controller of the amhSdp
 */
.controller('SaaSDMPhoenixFooterCtrl', function($scope, $rootScope, $app, $actions) {

	$scope.lfmenu = $actions.group('lfmenu');
	$scope.rfmenu = $actions.group('rfmenu');
	$scope.social = $actions.group('social');
	if($rootScope.app.config){		
		$scope.translateParams = {
				copyrightOwner : $rootScope.app.config.title
		};
	}
	
	
	function loadSocialLinks(){
		$scope.socialLinks = [];
		$app.config('sociallinks')//
		.then(function(links){
			if(!angular.isDefined(links)){
				return;
			}
			var link = null;
			if(links.facebook){
				link = createUrlLink('facebook', '', 'facebook', 3, links.facebook);
				$scope.socialLinks.push(link);
			}
			if(links.instagram){
				link = createUrlLink('instagram', '', 'wb-social-instagram', 3, links.instagram);
				$scope.socialLinks.push(link);
			}
			if(links.googleplus){
				link = createUrlLink('g+', '', 'wb-social-googleplus', 3, links.googleplus);
				$scope.socialLinks.push(link);
			}
			if(links.linkedin){
				link = createUrlLink('linkedin', '', 'linkedin', 3, links.linkedin);
				$scope.socialLinks.push(link);
			}
			if(links.twitter){		
				link = createUrlLink('twitter', '', 'twitter', 3, links.twitter);
				$scope.socialLinks.push(link);
			}
			if(links.telegram){
				link = createUrlLink('telegram', '', 'wb-social-telegram', 3, links.telegram);
				$scope.socialLinks.push(link);
			}
		});
	}

	function createUrlLink(label, description, icon, priority,
			url) {
		return {
			label : label,
			description : description,
			icon : icon,
			visible : function () {
				return true;
			},
			priority : priority,
			link : url
		};
	}
	
//	$scope.$watch('app.config.sociallinks', loadSocialLinks, true);
	
	loadSocialLinks();

});
