/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name AmdToolbarCtrl
 * @description Toolbar
 * 
 */
.controller('AmhSdpToolbarTopCtrl', function($scope, $actions, $navigator) {
	
	$scope.toolbarMenu = $actions.group('amhsdp.toolbar.top');
	
	var ctrl = {};
	
	function search(query) {
		if (query) {
			$navigator.openPage('assets/' + query);
		} else {
			$navigator.openPage('assets');
		}
	}
	
	/**
	 * Returns false if and only if item.visible === false or item.visible() === false.
	 * Otherwise returns true.
	 */
	function isVisible(item){
		if(angular.isFunction(item.visible)){
			return item.visible();
		}
		return angular.isDefined(item.visible) ? item.visible : true;
	}
	
	$scope.ctrl = ctrl;
	
	$scope.query = '';
	$scope.search = search;
	$scope.isVisible = isVisible;
	
});