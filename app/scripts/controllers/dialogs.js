/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

/**
 * @ngdoc function
 * @name amhSdp.controller:sdpDialogsCtrl
 * @description # sdpDialogsCtrl کنترلر پیش فرض برای تمام دریچه‌های محاوره‌ای.
 */
angular.module('amhSdp').//
controller('sdpDialogsCtrl', function($scope, $mdDialog, $location) {
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function(a) {
        $mdDialog.hide(a);
    };
    $scope.goto = function(path){
    	$mdDialog.cancel();
    	$location.path(path);
    };
});
