/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:AmhSdpPreferencesProfileOptionsCtrl
 * @description # SaaSDMPhoenixFooterCtrl Controller of the amhSdp
 */
.controller('AmhSdpPreferencesProfileOptionsCtrl', function($scope, $rootScope, $app, $navigator, $notification) {

	$app.config('profileoptions')//
	.then(function(options){
		if(typeof options === 'undefined'){
			$rootScope.app.config.profileoptions = {};
		}
		if(typeof options.educations === 'undefined'){
			$rootScope.app.config.profileoptions.educations = [];			
		}
		if(typeof options.familiar_mediums === 'undefined'){
			$rootScope.app.config.profileoptions.familiar_mediums = [];			
		}
		if(typeof options.activity_fields === 'undefined'){
			$rootScope.app.config.profileoptions.activity_fields = [];			
		}
		if(typeof options.favorite_items === 'undefined'){
			$rootScope.app.config.profileoptions.favorite_items = {};
		}else{
			reloadFavItems();
		}
	});

	function reloadFavItems(){
		$scope.favoriteItems = [];
		var favItems = $rootScope.app.config.profileoptions.favorite_items;
		for(var cat in favItems){
			var list = favItems[cat];
			for(var i=0; i<list.length; i++){
				$scope.favoriteItems.push({category: cat, name: list[i]});
			}
		}
	}
	
	function addFavorite(){
		$navigator.openDialog({
			templateUrl: 'views/dialogs/new-favorite-item.html',
			config: {
				model:{}
			}
		})//
		.then(function(model){
			var favItems = $rootScope.app.config.profileoptions.favorite_items;
			if(typeof favItems[model.category] === 'undefined'){
				favItems[model.category] = [];
			}
			if(favItems[model.category].indexOf(model.name) === -1){
				favItems[model.category].push(model.name);
			}
			return model;
		})//
		.then(function(){
			reloadFavItems();
		}, function(){
			$notification.alert('Fail to add new favorite item.');
		});
	}

	function removeFavorite(fav){
		var favItems = $rootScope.app.config.profileoptions.favorite_items;
		var list = favItems[fav.category];
		list.splice(list.indexOf(fav.name), 1);
		if(list.length === 0){
			delete favItems[fav.category];
		}
		reloadFavItems();
	}

	$scope.addFavorite = addFavorite;
	$scope.removeFavorite = removeFavorite;

});
