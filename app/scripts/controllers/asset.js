/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:AssetCtrl
 * @description # AssetCtrl Controller of the amhSdp
 */
.controller('AssetCtrl', 
		function($scope, $rootScope, $q, $sdp, $routeParams, $actions, $page, $navigator,
				QueryParameter) {

	$scope.content = {};
	$scope.relateds = [];
	$scope.relations = [];
	$scope.chain = [];
	$scope.working = true;
	$scope.error = 0;
	$scope.assetId = $routeParams.assetId;
	
	function loadRelatedAssets() {
		var pp = new QueryParameter();
		pp.setOrder('modif_dtime', 'd');
		return $sdp.relatedAssetsToAsset($scope.asset, pp)//
		.then(function(clist) {
			$scope.relateds = clist;
		});
	}
	
	function loadRelations() {
		var pp = new QueryParameter();
		pp.setOrder('modif_dtime', 'd');
		pp.setFilter('start', $scope.asset.id);
		return $sdp.assetRelations(pp)//
		.then(function(clist) {
			$scope.relations = clist;
		});
	}	
	
	
	function load() {
		$scope.content = {};
		var assetId;
		if ($routeParams.assetId) {
			assetId = $routeParams.assetId;
		} else {
			// error
			return;
		}
		$scope.working = true;
		return $sdp.asset(assetId)//
		.then(function(asset) {
			$scope.asset = asset;
			$scope.working = false;
			$scope.error = 0;
			return $scope.asset;
		}, function(error) {
			console.log('Fail to get asset: ' + error.data.message);
			$scope.error = error.status;
			$scope.working = false;
		})//
		.then(function(asset) {
			if(typeof asset === 'undefined'){
				return;
			}
			$scope.setSeoSetting('asset', $scope.asset);
			var promise1 = $scope.loadContentOfAsset(asset);
			// load related assets
			var promise3 = loadRelatedAssets();
			// load samples
			var promise4 = loadRelations();
			
			$q.all([promise1, promise3, promise4])//
			.then(function(){
				window.prerenderReady = true;
			})//
			.catch(function(){
				window.prerenderReady = true;				
			});
		});
	}

	function download(assetId){
		if(!assetId){
			return;
		}
		if($rootScope.app.user.anonymous){
			$scope.goTo('users/login');
			return;
		}
		$sdp.createLink(assetId)//
		.then(function(link) {
			$scope.goTo('asset/' + assetId + '/link/' + link.id);			
		}, function(error) {
			console.log('Fail to get link: ' + error.data.message);
			$scope.error = error.status;
			$scope.working = false;
			if(error.data.message !== undefined){
				$scope.errorMessage = error.data.message;
				$navigator.openDialog({
					templateUrl: 'views/dialogs/amd-alert.html',
					config: {
						message: error.data.message
					}
				});
			}
		});
	}
	
	/*
	 * Add scope menus
	 */
	$actions.newAction({ // edit content
		id: 'amhsdp.scope.edit_content',
		priority : 15,
		icon : 'edit',
		label: 'Edit content',
		title: 'Edit content',
		description : 'Toggle edit mode of the current contet',
		visible : function() {
			return $rootScope.app.user.owner;
		},
		action : $scope.toggleEditable,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	
	$actions.newAction({ // add new content
		id: 'amhsdp.scope.new_content',
		priority : 15,
		icon : 'add_box',
		label: 'Create new content',
		title: 'Create new content',
		description : 'Create new content',
		visible : function() {
			return $rootScope.app.user.owner;
		},
		action : $scope.createNewContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	$actions.newAction({ // save content
		id: 'amhsdp.scope.save_content',
		priority : 10,
		icon : 'save',
		label: 'Save current changes',
		title: 'Save current changes',
		description: 'Save current changes',
		visible : function() {
			return $rootScope.app.user.owner;
		},
		action : $scope.saveContent,
		groups: ['amh.owner-toolbar.scope'],
		scope: $scope
	});
	
	$scope.download = download;
	$scope.load = load;

	load();
	
	$scope.$watch('contentValue', function(){
		if($scope.contentValue !== undefined && $scope.contentValue !== null){
			$scope.setSeoSetting('content', $scope.contentValue);
		}
	}, true);
	
});
