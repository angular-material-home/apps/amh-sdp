/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:SignupCtrl
 * @memberof amhSdp
 * @description # SignupCtrl Controller of the amhSdp
 */
.controller('SignupCtrl', function($scope, $usr, $app, $window) {

	var ctrl = {
		myForm: {},
		createUser: false,
		loadUser: false,
		saveProfile : false,
		loadProfile : false,
		selectedActivities : [],
		selectedFavorites : []
	};
	$scope.ctrl = ctrl;
	
	/**
	 * عمل ثبت‌نام کاربر در سیستم را پیاده سازی می‌کند.
	 */
	function signup(data) {
		ctrl.createUser = true;
		$scope.signupMessage = '';
		data.login = data.email;
		return $usr.newUser(data)//
		.then(function () {
			ctrl.createUser = false;
			$scope.signupMessage = 'signup is successful';
			$app.login(data)//
			.then(function () {
				createUserProfile(data.profile);
			});
		}, function (error) {
			ctrl.createUser = false;
			if (error.status === 400) {
				$scope.signupMessage = 'username is existed';
			}else{
				$scope.signupMessage = 'signup is failed';				
			}
		});
	}

	function createUserProfile(data) {
		ctrl.saveProfile = true;
		return $app.currentUser()//
		.then(function (usr) {
			return usr;
		})//
		.then(function(user){
			return user.profile()//
			.then(function (profile) {
				return profile;
			});
		})//
		.then(function(profile){
			data.activities = ctrl.selectedActivities.join(',');
			data.favorites = ctrl.selectedFavorites.join(',');
			profile.setData(data);
			return profile.update()//
			.then(function(){
				ctrl.saveProfile = false;
				$scope.signupMessage = 'profile is created';
			});
		})//
		.catch(function () {
			ctrl.saveProfile = false;
			$scope.signupMessage = 'create profile is failed';
		});
	}

	/**
	 * Loads profile options. These settings are loaded from SPA config
	 */
	function loadProfileOptions(){
		$app.config('profileoptions')//
		.then(function(options){
			if(typeof options === 'undefined'){
				return;
			}
			ctrl.educations = options.educations;
			ctrl.familiar_mediums = options.familiar_mediums;
			ctrl.activity_fields = options.activity_fields;
			ctrl.favorite_items = options.favorite_items;
		})//
		.finally(function() {
			window.prerenderReady = true;
		});
	}
	
	function toggle(item, list) {
		if(typeof list === 'undefined'){
			list = [];
		}
		var idx = list.indexOf(item);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item);
		}
	}

	function exists(item, list) {
		if(typeof list === 'undefined'){
			list = [];
		}
		return list.indexOf(item) > -1;
	}
	
	$scope.back = function() {
		$window.history.back();
	};
	
	$scope.signup = signup;
	$scope.toggle = toggle;
	$scope.exists = exists;
	
	loadProfileOptions();
	
});
