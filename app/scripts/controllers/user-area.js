/*
 * @Deprecated
 * 
 * This controller is deprecated. Use MbAccountCtrl, MbProfileCtrl or MbPasswordCtrl in mblowfish-core version 1.1.0
 */

///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//angular.module('amhSdp')
//
///**
// * @ngdoc controller
// * @memberof amhSdp
// * @name UserAreaCtrl
// * @description # UserAreaCtrl
// * 
// */
//.controller('UserAreaCtrl', function($scope, $q, $navigator, $usr, $app, $window) {
//
//	var ctrl = {
//		loadUser: false,
//		loadProfile : false,
//		saveUser : false,
//		saveProfile : false,
//		user: null,
//		profile: null,
//	};
//	$scope.ctrl = ctrl;
//
//	/**
//	 * Sets a user in the scope
//	 * 
//	 * @param user
//	 * @returns
//	 */
//	function setUser(user){
//		ctrl.user = user;
//		// TODO: set page title
//	}
//
//	/**
//	 * Loads user data
//	 * @returns
//	 */
//	function loadUser(){
//		ctrl.loadUser = true;
//		return $app.currentUser()//
//		.then(function(user){
//			setUser(user);
//			ctrl.loadUser = false;
//			return user;
//		})//
//		.then(function(us){
//			ctrl.loadProfile = true;
//			return us.profile()//
//			.then(function(profile){
//				ctrl.profile = profile;
//				// activities
//				if(!ctrl.profile.activities){
//					ctrl.selectedActivities = [];
//				}else{					
//					ctrl.selectedActivities = ctrl.profile.activities.split(',');
//				}
//				// favorites
//				if(!ctrl.profile.favorites){
//					ctrl.selectedFavorites = [];
//				}else{					
//					ctrl.selectedFavorites = ctrl.profile.favorites.split(',');
//				}
//				ctrl.loadProfile = false;
//				return profile;
//			});
//		})//
//		.catch(function(){
//			ctrl.loadUser = false;
//			ctrl.loadProfile = false;
//		});
//	}
//
//	/**
//	 * Loads profile options. These settings are loaded from SPA config
//	 */
//	function loadProfileOptions(){
//		return $app.config('profileoptions')//
//		.then(function(options){
//			if(typeof options === 'undefined'){
//				return;
//			}
//			ctrl.educations = options.educations;
//			ctrl.familiar_mediums = options.familiar_mediums;
//			ctrl.activity_fields = options.activity_fields;
//			ctrl.favorite_items = options.favorite_items;
//		});
//	}
//	
//	/**
//	 * Save current user
//	 * 
//	 * @returns
//	 */
//	function saveUser(){
//		// TODO: maso, 2017: check if user exist
//		ctrl.saveUser = true;
//		return ctrl.user.update()//
//		.then(function(){
//			ctrl.saveUser = false;
//		}, function(error){
//			ctrl.error = error;
//			ctrl.saveUser = false;
//		});
//	}
//	
//	/**
//	 * Save current user
//	 * 
//	 * @returns
//	 */
//	function saveProfile(){
//		ctrl.saveProfile = true;
//		ctrl.profile.activities = ctrl.selectedActivities.join(',');
//		ctrl.profile.favorites = ctrl.selectedFavorites.join(',');
//		return ctrl.profile.update()//
//		.then(function(){
//			ctrl.saveProfile = false;
//		})
//		.catch(function(error){
//			ctrl.error = error;
//			ctrl.saveProfile = false;
//		});
//	}
//
//	function toggle(item, list) {
//		if(typeof list === 'undefined'){
//			list = [];
//		}
//		var idx = list.indexOf(item);
//		if (idx > -1) {
//			list.splice(idx, 1);
//		}
//		else {
//			list.push(item);
//		}
//	}
//
//	function exists(item, list) {
//		if(typeof list === 'undefined'){
//			list = [];
//		}
//		return list.indexOf(item) > -1;
//	}
//
//
//	function changePassword(data) {
//		var param = {
//			'old' : data.oldPass,
//			'new' : data.newPass
//		};
//		return $usr.resetPassword(param)//
//		.then(function(){
//			logout();
//		}, function(error){
//			alert('Fail to update password:'+error.data.message);
//		});
//	}
//	
//	function logout(){
//		$usr.logout()//
//		.then(function(){
//			$navigator.openPage('users/login');
//		});
//	}
//	
//	function updateAvatar(avatarFiles){
//		// XXX: maso, 1395: reset avatar
//		return ctrl.user.newAvatar(avatarFiles[0].lfFile)//
//		.then(function(){
//			$window.location.reload();
//		}, function(error){
//			alert('Fail to update avatar:' + error.data.message);
//		});
//	}
//
//	// Account property descriptor
//	$scope.apds = [ {
//		key : 'first_name',
//		title : 'First name',
//		type : 'string'
//	}, {
//		key : 'last_name',
//		title : 'Last name',
//		type : 'string'
//	}, {
//		key : 'language',
//		title : 'language',
//		type : 'string'
//	}, {
//		key : 'timezone',
//		title : 'timezone',
//		type : 'string'
//	} ];
//	/**
//	 * Cancel page
//	 * @returns
//	 */
//	function cancel(){
//		$navigator.openPage('/');
//	}
//	
//	$scope.load = loadUser;
//	$scope.save = saveProfile;
//	$scope.toggle = toggle;
//	$scope.exists = exists;
//	
//	$scope.saveUser = saveUser;
//	$scope.cancel = cancel;
//	$scope.changePassword = changePassword;
//	$scope.updateAvatar = updateAvatar;
//
//	var promise1 = loadUser();
//	var promise2 = loadProfileOptions();
//	$q.all([promise1, promise2])//
//	.then(function(){
//		window.prerenderReady = true;
//	})//
//	.catch(function(){
//		window.prerenderReady = true;				
//	});
//
//});
