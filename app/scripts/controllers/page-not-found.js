/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name amhSdp.controller:SdpPageNotFoundCtrl
 * @description # SdpPageNotFoundCtrl Controller of the amhSdp
 */
.controller('SdpPageNotFoundCtrl', 
		function($scope, $app, $page) {

	var ctrl = {
		loading : false	
	};
	
	function load(){
		ctrl.loading = true;
		$app.config('pageNotFound')//
		.then(function(value){
			$scope.setSeoSetting('content', value);
		})//
		.finally(function(){
			$page.setMeta('prerender-status-code', '404');
			window.prerenderReady = true;
			ctrl.loading = false;
		});
	}

	$scope.ctrl = ctrl;
	$scope.load = load;

	load();

});
