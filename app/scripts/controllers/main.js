/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')//

/**
 * @ngdoc controller
 * @name amhSdp.controller:MainCtrl
 * @memberof amhSdp
 * @description # MainCtrl Controller of the amhSdp
 * 
 * کنترلر اصلی سیستم هست که کل نرم افزار را مدیریت می‌کند. این کنترل در
 * صفحه اصلی به کار گرفته شده و سایر بخش‌های سیستم به عنوان بخش‌هایی از
 * این کنترل در نظر گرفته می‌شود.
 * 
 * این کنترل در فایل اصلی سیستم استفاده می‌شه و تنها یکبار زمان اجرای
 * سیستم فراخوانی خواهد شد. استفاده از این کنترل در جاهای دیگه سیستم
 * منجر به بهم ریختگی سیستم خواهد شد.
 * 
 * @deprecated this controller is not required any more
 */
.controller('MainCtrl', function ($scope, $rootScope, $routeParams, $cms, $translate, 
		$page, $navigator, $mdSidenav, $location) {

	function openMenu($mdMenu, ev) {
		// originatorEv = ev;
		$mdMenu.open(ev);
	}

	/**
	 * این تابع برای تغییر زبان سیستم به زبان مشخص استفاده
	 * می‌شود. کلید زبان مورد نظر باید در سیستم تعریف شده باشد.
	 * 
	 * @param {String}
	 *            key کلید زبان مورد نظر
	 * @return {promise} دستگیره برای اجرا
	 */
	function changeLanguage(key) {
		return $translate.use(key);
	}

	/**
	 * به صفحه دیگر با آدرس داده شده می‌رود
	 */
	function goTo(path) {
		$location.path(path);
	}

	$scope.openMenu = openMenu;
	$scope.goTo = goTo;
	$scope.changeLanguage = changeLanguage;

	// ***********************************************
	// Methods for contents
	// ***********************************************

	function setSeoSetting(itemType, item){
		var title, description, keywords;
		switch (itemType) {
		case 'asset':
			title = item.name;
			description = item.description;
			break;
		case 'category':
			title = item.name;
			description = item.description;
			break;
		case 'content':
			title = item.label;
			description = item.description;
			keywords = item.keywords;
//			favicon = item.cover;
			break;
		case 'collection':
			title = item.title;
			description = item.description;
			break;
		}
		if(title !== undefined && title !== null && title !== ''){
			$page.setTitle(title);
		}
		if(description !== undefined && description !== null && description !== ''){
			$page.setDescription(description);
		}
		if(keywords !== undefined && keywords !== null && keywords !== ''){
			$page.setKeywords(keywords);
		}
//		if(favicon !== undefined)
//		$page.setFavicon(favicon);
	}

	function toggleEditable() {
		$scope.ngEditable = !$scope.ngEditable;
	}

	/**
	 * یک محتوا برای استفاده در این نمایش ایجاد می‌کند. این محتوی
	 * نامدار با استفاده از سرویس مدیریت محتوی ایجاد خواهد شد.
	 * 
	 * برای ایجاد محتوا یه پنجره به کاربر نشان می دهد تا برخی فیلدها را وارد کند
	 */
	function createNewContent() {
		var name = null;
		var data = {
				title : 'title',
				description : 'Content created from SaaSDM-Phoenix main controller',
				mime_type : 'application/weburger+json',
				file_name : $scope.name + '.json'
		};
		if(!$scope.ctrl.realPage){
			data.name = _getContentName();
		}
		// Update data by user
		$navigator.openDialog({
			templateUrl : 'views/dialogs/amh-content.html',
			config : {
				data : data
			}
		})
		// Create content
		.then(function(contentData){
			name = contentData.name;
			contentData.name = contentData.name + '-' + $translate.use();
			return $cms.newContent(contentData);
		})//
		.then(function(newContent) {
			$scope.ctrl.status = 'ok';
			newContent.setValue({});
			return newContent;
		}, function() {
			alert('fail to create the content');
		});
	}

	function _createContent(contentName) {
		var data = {
				name : contentName,
				title : 'SDP content (' + contentName + ')',
				description : 'Content created from SaaSDM-Phoenix main controller.',
				mime_type : 'application/json',
				file_name : contentName + '.json'
		};
		return $cms.newContent(data)//
		.then(function (myContent) {
			$scope.content = myContent;
			$scope.content.setValue({});
			$scope.contentValue = {};
			return $scope.content;
		});
	}

	function _getContentName() {
		return ($routeParams.name || $rootScope.app.key);
	}

	function loadContentOfAsset(asset) {
		$scope.content = {};
		var myContent = asset.content ? asset.content
				: 'asset-content-' + asset.id;
		return $cms.content(myContent)//
		.then(function (nc) {
			return nc;
		}, function () {
			// Create content if not exist or error is occured
			return _createContent('asset-content-' + asset.id)//
			.then(function (nc) {
				asset.content = nc.id;
				asset.update();
				return nc;
			}, function () {
				// do nothing
			});
		})//
		.then(function (nc) {
			if (typeof nc !== 'undefined') {
				$scope.ctrl.realPage = true;
				$scope.content = nc;
				nc.value()
				.then(function (val) {
					if(val === null || val === ''){
						$scope.content.setValue({});
						$scope.contentValue = {};
					}else{                		
						$scope.contentValue = val;
					}
				});
			}
		});
	}

	function loadContentOfCategory(category) {
		$scope.content = {};
		var myContent = category.content ? category.content
				: 'category-content-' + category.id;
		return $cms.content(myContent)//
		.then(function (nc) {
			return nc;
		}, function () {
			return _createContent('category-content-' + category.id)//
			.then(function (nc) {
				category.content = nc.id;
				category.update();
				return nc;
			}, function () {
				// do nothing
			});
		})//
		.then(function (nc) {
			if (typeof nc !== 'undefined') {
				$scope.ctrl.realPage = true;
				$scope.content = nc;
				nc.value()
				.then(function (val) {
					if(val === null || val === ''){
						$scope.content.setValue({});
						$scope.contentValue = {};
					}else{
						$scope.contentValue = val;
					}
				});
			}
		});
	}

	/**
	 * Save or update content
	 * @returns
	 */
	function saveContent() {
		return $scope.content.setValue($scope.contentValue)//
		.then(function() {
			alert('content is saved successfully');
		}, function(){
			alert('saving content is failed');
		});
	}

	$scope.ngEditable = false;
	$scope.toggleEditable = toggleEditable;
	$scope.setSeoSetting = setSeoSetting;
	$scope.content = null;
	$scope.contentValue = null;
	$scope.createNewContent = createNewContent;
	$scope.saveContent = saveContent;
	$scope.loadContentOfAsset = loadContentOfAsset;
	$scope.loadContentOfCategory = loadContentOfCategory;

	$scope.ctrl = {
			realPage: false
	};

	$scope.languages = [ {
		title : 'persian',
		key : 'fa',
	} ];

	$rootScope.toggleSidenav = function (id) {
		$mdSidenav(id).toggle();
	};

});
