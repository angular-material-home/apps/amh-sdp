'use strict';
angular.module('amhSdp')

/**
 * @ngdoc function
 * @name amhSdp.controller:AmdNavigatorDialogCtrl
 * @description # AmdNavigatorDialogCtrl Controller of the amhSdp
 */
.controller('AmdNavigatorDialogCtrl', function($scope, $mdDialog, config) {
    $scope.config = config;
    $scope.hide = function() {
	$mdDialog.hide();
    };
    $scope.cancel = function() {
	$mdDialog.cancel();
    };
    $scope.answer = function(a) {
	$mdDialog.hide(a);
    };
});
