/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name AmhSdpSidenavMainCtrl
 * @description Toolbar
 * 
 */
.controller('AmhSdpSidenavMainCtrl', function($scope, $actions, $sdp, QueryParameter) {
	
	$scope.sidenav = $actions.group('amhsdp.sidenav.main');
	$scope.userSpace = $actions.group('amhsdp.user-space');
	
	var ctrl = {};
	
	function loadRootCategories() {
		if(ctrl.loadingCategories){
			return false;
		}
		ctrl.loadingCategories = true;
		// prepare paginator parameter
		var pp = new QueryParameter();
		pp.setFilter('parent', '0');
		pp.setSize(-1);
		pp.setOrder('id', 'a');
		return $sdp.categories(pp)//
		.then(function (rootCats) {
			$scope.rootCategories = rootCats.items;
			return rootCats.items;
		}, function () {
			$scope.rootCategories = [];
		})//
		.finally(function(){
			ctrl.loadingCategories = false;
		});
	}
	
	/**
	 * Returns false if and only if item.visible === false or item.visible() === false.
	 * Otherwise returns true.
	 */
	function isVisible(item){
		if(angular.isFunction(item.visible)){
			return item.visible();
		}
		return angular.isDefined(item.visible) ? item.visible : true;
	}
	
	$scope.ctrl = ctrl;
	
	$scope.isVisible = isVisible;
	
	loadRootCategories();
	
});