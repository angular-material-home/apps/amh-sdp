/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc controller
 * @name AmdToolbarCtrl
 * @description Toolbar
 * 
 */
.controller('AmhSdpToolbarMainCtrl', function($scope, $mdSidenav, $translate, 
		$actions, $navigator, $sdp, $collection, QueryParameter) {
	
	$scope.toolbarMenu = $actions.group('amhsdp.toolbar.main');
	
	var ctrl = {};
	
//	function loadRootItems() {
//		ctrl.loadRoot = true;
//		// prepare paginator parameter
//		var pp = new QueryParameter();
//		pp.setFilter('parent', '0');
//		pp.setSize(20);
//		pp.setOrder('id', 'd');
//		// load assets
//		return $sdp.assets(pp)//
//		.then(function (rootItems) {
//			$scope.rootItems = rootItems.items;
//		}, function (error) {
//			alert('Fail to get root items: ' + error.data.message);
//			$scope.rootItems = [];
//			ctrl.loadRoot = false;
//		});
//	}

	function loadRootCategories() {
		if(ctrl.loadingCategories){
			return false;
		}
		ctrl.loadingCategories = true;
		// prepare paginator parameter
		var pp = new QueryParameter();
		pp.setFilter('parent', '0');
		pp.setSize(-1);
		pp.setOrder('id', 'a');
		return $sdp.categories(pp)//
		.then(function (rootCats) {
			$scope.rootCategories = rootCats.items;
			return rootCats.items;
		}, function () {
			toast($translate.instant('Fail to get categories.'));
			$scope.rootCategories = [];
		})//
		.finally(function(){
			ctrl.loadingCategories = false;
		});
	}

	function loadEvents() {
		if(ctrl.loadingEvents){
			return false;
		}
		ctrl.loadingEvents = true;
		// load event menu collection
		return $collection.collection('__event_menu_collection__')//
		.then(function(collection){
			// prepare paginator parameter
			var pp = new QueryParameter();
			pp.setSize(-1);
			pp.setOrder('id', 'a');
			return collection.documents(pp);//
		})
		.then(function (eventsPag) {
			var items = eventsPag.items;
			$scope.events = [];
			var i;
			for(i=0 ; i<items.length ; i++){
				var ev = items[i];
				$scope.events.push({
					title : ev.title,
					url : 'events/' + ev.collection_name
				});
			}
		})//
		.finally(function () {
			ctrl.loadingEvents = false;
		});
	}
	
	function load() {
		// load root items
//		loadRootItems();
		// load root categories
		loadRootCategories();
		// load collections
		loadEvents();
	}
	
	function find(query) {
		if (query) {
			$navigator.openPage('assets/' + query);
		} else {
			$navigator.openPage('assets');
		}
	}
	
	function toggleSidenav(){
		$mdSidenav('amh-sdp.main-sidenav').toggle();
	}
	
	$scope.ctrl = ctrl;
	
	$scope.query = '';
	$scope.search = find;
	$scope.toggleSidenav = toggleSidenav;
	
	load();
	
});