/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('amhSdp')

    /**
     * @ngdoc directive
     * @name wbUiSettingSearchableSelect
     * @memberof amhSdp
     * @description a setting section for search and choosing values from a list
     *
     */
    .directive('wbUiSettingSearchableSelect', function () {
    	
    	function link(scope, element){
    		// The md-select directive eats keydown events for some quick select
    		// logic. Since we have a search input here, we don't need that logic.
    		element.find('input').on('keydown', function(ev) {
    			ev.stopPropagation();
    		});
    		if(typeof scope.searchable === 'undefined' || scope.searchable === null){
    			scope.searchable=true;
    		}
    	}
    	
        return {
            templateUrl: 'views/directives/wb-ui-setting-searchable-select.html',
            restrict: 'EA',
            replace: true,
            scope: {
                label: '@',
                searchPlaceholder: '@',
                value: '=ngModel',
                options:'=',
                searchable : '='
            },
        	link:link
        };
    });
