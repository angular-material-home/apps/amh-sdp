/* jslint todo: true */
/* jslint xxx: true */
/* jshint -W100 */
'use strict';

angular.module('amhSdp')

/**
 * @ngdoc directive
 * @name amhSdp.directive:sdpAssetFilter
 * @description # sdpAssetFilter
 */
.directive('sdpAssetFilter', function() {
	return {
		restrict : 'E',
		templateUrl : 'views/directives/assetfilter.html',
		require : '^ngModel',
		scope : {
			ngModel : '=?',
			action : '=?',
			direction : '=?'
		},
		controller : function() {

		}
	};
});
