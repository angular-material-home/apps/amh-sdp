# AMH-SDP (Angular Material Home - Sell Digital Products)

[![pipeline status](https://gitlab.com/angular-material-home/apps/amh-sdp/badges/master/pipeline.svg)](https://gitlab.com/angular-material-home/apps/amh-sdp/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/d86ccee874624288b01b388bd150ab0c)](https://www.codacy.com/app/amh-apps/amh-sdp?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-home/apps/amh-sdp&amp;utm_campaign=Badge_Grade) 
[![Codacy Badge](https://api.codacy.com/project/badge/Coverage/d86ccee874624288b01b388bd150ab0c)](https://www.codacy.com/app/amh-apps/amh-sdp?utm_source=gitlab.com&utm_medium=referral&utm_content=angular-material-home/apps/amh-sdp&utm_campaign=Badge_Coverage)

این پروژه یک SPA برای فراهم کردن امکانات زیر است:

- مشاهده فهرست asset ها
- مشاهده جزییات هر asset
- جستجو در asset ها
- دانلود asset مورد نظر

This project is generated with yo angular generator

## ساخت و توسعه

### ساختار

این پروژه با استفاده از yo angular generator ساختاردهی شده است.

### ساخت و پیش‌نمایش

برای ایجاد محصول نهایی پروژه دستور `grunt` را اجرا کنید.

برای پیش‌نمایش پروژه دستور `grunt serve` اجرا کنید.

## تست

دستور `grunt test` unit test ها را با karma اجرا می‌کند.
